import java.util.*;
public class MapRoute {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        g = new Vertex[n][n];
        for(int i = 0; i<n; i++) {
            for (int j = 0 ; j < n; j++) {
                g[i][j] = new Vertex(in.nextInt());
            }
        }
        NextAdd(n);
        dijkstra(n);
    }

    public static  PriorityQueue<Vertex> q;
    public static   Vertex[][] g;
    private static int res;

    static class Vertex implements Comparable<Vertex>{
        ArrayList<Vertex> next = new ArrayList<>();
        int dist = Integer.MAX_VALUE, u;
        int index = 0;
        public Vertex(int u){
            this.u = u;
        }
        @Override
        public int compareTo(Vertex x) {
            return this.dist-x.dist;
        }
    }

    public static void NextAdd(Integer n) {
        for(int i = 0; i<n; i++){
            for(int j= 0; j<n; j++){
                if(i+1<n) g[i][j].next.add(g[i+1][j]);
                if(j+1<n) g[i][j].next.add(g[i][j+1]);
                if(i>0) g[i][j].next.add(g[i-1][j]);
                if(j>0) g[i][j].next.add(g[i][j-1]);
            }
        }
    }

    private static boolean relax(Vertex v, Vertex u) {
        boolean changed = (v.dist+u.u<u.dist);
        return changed;
    }

    public static void dijkstra(Integer n){
        q = new PriorityQueue();
        g[0][0].dist = 0;
        q.add(g[0][0]);
        while(!q.isEmpty()){
            Vertex v = q.poll();
            v.index = -1;
            for (Vertex u : v.next) {
                if  (relax(v,u) &&  u.index!=-1) {
                    u.dist = v.dist+u.u;
                    q.add(u);
                }
            }
            res = g[n-1][n-1].dist+g[0][0].u;
        }
        System.out.print(res);
    }
}