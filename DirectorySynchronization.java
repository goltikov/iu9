import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Sync {

    private static ArrayList<String> Del, Copy,Help;
    private static File S,D;
    private static String T;

    public static void main(String[] args) throws IOException {
         S = new File(args[0]);
         D = new File(args[1]);
         T = args[0];
         Del=new ArrayList<>();
         Copy=new ArrayList<>();
         Help=new ArrayList<>();



        Adder(S);
        //System.out.println(Copy);
        Compare(D);

        if (Copy.isEmpty() && Del.isEmpty()) {
            System.out.println("IDENTICAL");
            System.exit(0);
        }
        else {
                Collections.sort(Del);
            for (int i = 0; i < Del.size() ; i++) {
                System.out.println("DELETE " + Del.get(i).substring(T.length() + 1));
            }
                Collections.sort(Copy);
            for (String a : Copy) {
                System.out.println("COPY " + a);
            }
        }
    }

    private static void Adder(File arg) throws IOException {
        for (String i : arg.list()) {
            File one = new File(arg, i);
            if (one.isDirectory()) Adder(one);
            else Copy.add(one.getPath().substring(T.length() + 1));
        }
    }


    private static void Compare(File arg) throws IOException {
        for (String i : arg.list()) {
            File one = new File(arg, i);
            if (one.isDirectory()) Compare(one);
            else {
                Help.add(one.getPath());
                String help = Help.get(0).substring(T.length() + 1);
                if (Copy.contains(Help.get(0).substring(T.length() + 1))) { ///
                    byte[] a = Files.readAllBytes(Paths.get(one.getAbsolutePath()));
                    byte[] b = Files.readAllBytes(Paths.get(T + File.separator + help ));////
                    if (Arrays.equals(a, b)) {
                        Copy.remove(one.getPath().substring(T.length() + 1));
                    } else {
                        Del.add(one.getPath());
                    }
                }
                else {
                    Del.add(one.getPath());
                }
                Help.remove(one.getPath());
            }
        }
    }

}